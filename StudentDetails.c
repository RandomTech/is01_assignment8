#include <stdio.h>
#include <string.h>

struct Student{
	char fname[20];
	char subject[20];
	int marks;
}; 

struct Student getInfo();
void printInfo(struct Student std);

int main(){
	struct Student arrstd[5];
	
	for(int i=0; i<5; i++)
	 arrstd[i] = getInfo();
 
	for(int j=0; j<5; j++){
		printf("Student %d Details\n", j+1);
		printInfo(arrstd[j]);
		printf("\n");
	}
	return 0;
}

struct Student getInfo(){
	struct Student std;
	
	printf("Enter Student Name: ");
	scanf("%s", std.fname);
	printf("Enter Subject Name: ");
	scanf("%s", std.subject);
	printf("Enter Subject Marks: ");
	scanf("%d", &std.marks);
	printf("\n");
	return std;
}
void printInfo(struct Student std){
	printf("Student Name: %s\n", std.fname);
	printf("Subject Name: %s\n", std.subject);
	printf("Marks Obtained: %d\n", std.marks);
}